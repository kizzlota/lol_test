import os
from celery import Celery
from celery.schedules import crontab  # pylint: disable=E0611
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

app = Celery('test_kisslota')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS, force=True)

app.conf.beat_schedule = {
    'cryptonator_currencies_fetcher': {
        'task': 'currencies.tasks.currencies_fetcher',
        'schedule': 10.0,
        'options': {'queue': 'normal', 'routing_key': 'normal'},
    },
}
