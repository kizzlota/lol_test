from django.apps import AppConfig


class CurrencyConfig(AppConfig):
    name = 'apps.currencies'

    def ready(self):
        super().ready()
        from currencies.tasks import currencies_fetcher
        from currencies.tasks import upload_abbreviations
        upload_abbreviations()