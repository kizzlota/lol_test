from django.urls import path, re_path
from currencies import views
urlpatterns = [

    path('currencies/', views.currencies_list_create_viewset),
    path('currency/<int:pk>/', views.currencies_update_delete_viewset),

]
