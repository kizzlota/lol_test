from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from apps.currencies.models import CurrencyAbbreviation
from django.db.models import Subquery
from currencies.utils import fetch_course
from django.db.utils import IntegrityError
import cryptonator


class DeleteAllCurrenciesMixin(generics.DestroyAPIView):
    def delete(self, request, *args, **kwargs):
        '''
        method that delete all curriencies.
        :param request: no
        :param args: no
        :param kwargs: no
        :return: status 204, successfull message
        '''
        all_currencies = self.get_queryset().all()
        [currency.delete() for currency in all_currencies]
        return Response({"OK": "Successfully deleted all entries"}, status=status.HTTP_204_NO_CONTENT)


class DeleteSpecificCurrencyMixin(generics.DestroyAPIView):
    def destroy(self, request, *args, **kwargs):
        '''
        to prevent return empty message. described full delete method.
        :param request: currency id
        :param args: no
        :param kwargs: no
        :return: status 204 and success message.
        '''
        instance = self.get_object()
        if instance:
            instance.delete()
            return Response({"OK": "Successfully deleted"}, status=status.HTTP_204_NO_CONTENT)
        return Response({"details": "No data found"}, status=status.HTTP_400_BAD_REQUEST)


class DetailsSpecificCurrencyMixin(generics.RetrieveAPIView):
    pass


class PatchSpecificCurrencyMixin(generics.UpdateAPIView):
    def patch(self, request, *args, **kwargs):
        '''
        updates currenicies name if user want new data
        :param request: json body
        :param args: id of currency
        :param kwargs: id of currency
        :return: serialized data, status 200
        '''
        instance = self.get_object()
        first_currency = request.data.get('first_currency').upper()
        second_currency = request.data.get('second_currency').upper()
        instance_queryset = self.get_queryset().filter(id=instance.id).annotate(
            abbrs=Subquery(CurrencyAbbreviation.objects.all().values_list('abbreviations', flat=True))).first()
        if first_currency in instance_queryset.abbrs and second_currency in instance_queryset.abbrs:
            try:
                course = fetch_course(first_currency=first_currency, second_currency=second_currency)
            except cryptonator.CryptonatorException as e:
                return Response({"details": str(e)}, status=status.HTTP_400_BAD_REQUEST)
            request.data.update({"course": course})
            serializer = self.get_serializer(data=request.data, instance=instance)
            serializer.is_valid(raise_exception=True)
            try:
                serializer.save()
            except IntegrityError:
                msg = {
                    "details": "this pair could not be created because another pairs has the same currency pairs"
                               " please choose anothe combination"}
                return Response(msg, status=status.HTTP_400_BAD_REQUEST)
            else:
                if serializer.errors:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                return Response(serializer.data, status=status.HTTP_200_OK)
