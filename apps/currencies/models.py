from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField

from model_utils.models import (
    TimeStampedModel,
    StatusModel,
    SoftDeletableModel,
)
User = get_user_model()


class CurrencyAbbreviation(models.Model):
    abbreviations = ArrayField(
        base_field=models.CharField(max_length=10),
        help_text=f'available currency shortcuts',
        null=True,
        blank=True
    )


class Currency(TimeStampedModel):
    class Meta:
        verbose_name = 'Currency pairs'
        verbose_name_plural = 'Currency pairs'
        unique_together = ('first_currency', 'second_currency')

    first_currency = models.CharField(verbose_name='First currency', max_length=15)
    second_currency = models.CharField(verbose_name='Second currency', max_length=15)
    course = models.DecimalField(verbose_name='Course', decimal_places=8, max_digits=25, null=True, blank=True)

    def __str__(self):
        return f"{self.first_currency} to {self.second_currency}"

    def __repr__(self):
        return f"{self.id} | pair {self.first_currency} to {self.second_currency}"

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.first_currency = self.first_currency.upper()
        self.second_currency = self.second_currency.upper()
        return super().save(force_insert, force_update, using, update_fields)
