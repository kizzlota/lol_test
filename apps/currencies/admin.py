from django.contrib import admin
from apps.currencies.models import (Currency, CurrencyAbbreviation)


# Register your models here.

@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_currency', 'second_currency', 'course')
    list_filter = ('first_currency', 'second_currency')
    readonly_fields = ('course',)

    def has_change_permission(self, request, obj=None):
        return False

@admin.register(CurrencyAbbreviation)
class CurrencyAbbreviationAdmin(admin.ModelAdmin):
    list_display = ('id',)