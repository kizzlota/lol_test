from rest_framework import generics
from rest_framework import permissions
from apps.currencies.models import (
    Currency,
    CurrencyAbbreviation
)
from currencies.serializers import CurrencySimpleSerializer
from currencies.views_mixins import (
    DeleteAllCurrenciesMixin,
    DeleteSpecificCurrencyMixin,
    DetailsSpecificCurrencyMixin,
    PatchSpecificCurrencyMixin,
)

__all__ = [
    'currencies_list_create_viewset',
    'currencies_update_delete_viewset',
]


class CurrenciesApiView(DeleteAllCurrenciesMixin, generics.ListCreateAPIView):
    http_method_names = ('get', 'post', 'delete', 'head', 'options')
    pagination_class = None
    queryset = Currency.objects
    serializer_class = CurrencySimpleSerializer
    permission_classes = (permissions.AllowAny,)


class CurrenciesUpdateDeleteApiView(DeleteSpecificCurrencyMixin,
                                    DetailsSpecificCurrencyMixin,
                                    PatchSpecificCurrencyMixin):
    http_method_names = ('delete', 'patch', 'get', 'head', 'options')
    pagination_class = None
    queryset = Currency.objects
    serializer_class = CurrencySimpleSerializer
    permission_classes = (permissions.AllowAny,)


currencies_list_create_viewset = CurrenciesApiView.as_view()
currencies_update_delete_viewset = CurrenciesUpdateDeleteApiView.as_view()
