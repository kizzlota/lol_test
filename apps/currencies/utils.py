from rest_framework.views import APIView
from rest_framework.serializers import BaseSerializer
import cryptonator


def get_keyword_argument(instance, keyword):
    """
    Returns keyword argument by keyword.
    :param instance: instance of <rest_framework.serializers.BaseSerializer>
        or <rest_framework.views.APIView>
    :param keyword: keyword argument from url
    :return: id as <int> or None if operation is failed
    """
    kwargs = {}
    if isinstance(instance, BaseSerializer):
        kwargs = instance.context.get('view').kwargs
    elif isinstance(instance, APIView):
        kwargs = instance.kwargs
    if keyword in kwargs:
        try:
            return int(kwargs.get(keyword))
        except TypeError:
            pass
    return None


def get_currency_id(instance):
    return get_keyword_argument(instance, 'pk')


def fetch_course(first_currency, second_currency):
    return cryptonator.get_exchange_rate(first_currency, second_currency)
